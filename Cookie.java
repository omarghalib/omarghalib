
public class Cookie extends DessertItem {
	private int price_per_dozen;
	private int number;
	public Cookie(String c, int n, int p) {
		price_per_dozen = p;
		super.name = c ;
		number = n;
	}
	@Override
	public int getCost() {
		int cost = price_per_dozen * number / 12;
		return cost;
	}
	
}
