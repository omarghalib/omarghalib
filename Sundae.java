
public class Sundae extends IceCream {
	private int topping_price;
	Sundae(String type, int p, String topping, int t_price) {
		super(type, p);
		topping_price = t_price;
		super.name = topping + " Sundae with " + super.name;
	}

	@Override
	public int getCost() {
		return (super.getCost()+topping_price);
	}
}
