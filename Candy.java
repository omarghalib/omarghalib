
public class Candy extends DessertItem {
	private double weight;
	private float  price_per_pound;
	Candy(String C_type,double d,float p) {
		weight = d;
		price_per_pound = p;
		super.name = C_type + " Candy";
	}
	@Override
	public int getCost() {
		//calculates the price
		int cost = (int) (weight * price_per_pound);
		return (cost);
	}
}
