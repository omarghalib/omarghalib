import java.util.ArrayList;

public class Checkout {
	private ArrayList<DessertItem> Checkout_list;
	public Checkout() {
		 Checkout_list = new ArrayList<DessertItem>();
	}
	public int numberOfItems() {
		return(Checkout_list.size());
	}
	public void enterItem(DessertItem item) {
		Checkout_list.add(item);
	}
	public void clear() {
		Checkout_list.clear();
	}
	public int totalCost() {
		int total = 0;
		for(int i = 0; i < Checkout_list.size(); i++) {
			total += Checkout_list.get(i).getCost();
		}
		return total;
	}
	public int totalTax() {
		int total_tax = (int) (this.totalCost() * DessertShoppe.TAX_RATE / 100);
		return(total_tax);
	}
	public java.lang.String toString(){
		String Check = null;
		Check += DessertShoppe.STORE_NAME;
		Check += "\r\n" + "----------------------";
		Check += "\r\n" + " ";
		
		for (int i = 0; i < this.numberOfItems(); i++) {
			Check += "\r\n" + Checkout_list.get(i).name + "     " + DessertShoppe.cents2dollarsAndCents(Checkout_list.get(i).getCost());
		}
		Check +="\r\n" + "Tax    " + DessertShoppe.cents2dollarsAndCents(this.totalTax());
		Check +="\r\n" + "Total Cost    " + DessertShoppe.cents2dollarsAndCents(this.totalTax() + this.totalCost());
		return Check;
	}
}
